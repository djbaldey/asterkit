#
# Copyright (c) 2022, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import unittest
from concurrent.futures import ThreadPoolExecutor
from asterkit.ami import Commander, Listener


class TestListener(unittest.TestCase):

    def test_attrs(self):
        listener = Listener()
        self.assertEqual(listener.host, 'localhost')
        self.assertEqual(listener.port, 5038)
        self.assertEqual(listener.username, 'test')
        self.assertEqual(listener.password, 'test')
        self.assertIsNone(listener.reader)
        self.assertIsNone(listener.writer)
        self.assertFalse(listener.is_authenticated)
        self.assertFalse(listener.is_connected)

        self.assertEqual(listener.pool_executor, ThreadPoolExecutor)
        self.assertEqual(listener.handlers, {})
        self.assertFalse(listener.is_listening)
        self.assertEqual(listener.skip_events, set())

    def test_skip_event(self):
        listener = Listener()
        self.assertNotIn('TestEvent', listener.skip_events)
        listener.skip_events.add('TestEvent')
        self.assertIn('TestEvent', listener.skip_events)

    def test_register_event(self):
        listener = Listener()

        async def handle_event(event):
            pass

        self.assertNotIn('*', listener.handlers)
        self.assertTrue(listener.register_event('*', handle_event))
        self.assertFalse(listener.register_event('*', handle_event))
        self.assertEqual(listener.handlers['*'], [handle_event])
        self.assertTrue(listener.unregister_event('*', handle_event))
        self.assertFalse(listener.unregister_event('*', handle_event))
        self.assertEqual(listener.handlers['*'], [])


class TestCommander(unittest.TestCase):

    def test_attrs(self):
        commander = Commander()
        self.assertEqual(commander.host, 'localhost')
        self.assertEqual(commander.port, 5038)
        self.assertEqual(commander.username, 'test')
        self.assertEqual(commander.password, 'test')
        self.assertIsNone(commander.reader)
        self.assertIsNone(commander.writer)
        self.assertFalse(commander.is_authenticated)
        self.assertFalse(commander.is_connected)
