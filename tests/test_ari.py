#
# Copyright (c) 2022, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import unittest

from asterkit.ari import Client


class TestClient(unittest.TestCase):

    def test_attrs(self):
        client = Client()

        self.assertFalse(client.ssl)
        self.assertEqual(client.host, 'localhost')
        self.assertEqual(client.port, 8088)
        self.assertEqual(client.username, 'asterisk')
        self.assertEqual(client.password, 'asterisk')
        self.assertEqual(client.reconnection_timeout, 1.0)
        self.assertEqual(client.event_models, {})
        self.assertFalse(client.is_connected)
