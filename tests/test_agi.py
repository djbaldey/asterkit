#
# Copyright (c) 2022, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import unittest

from asterkit.agi import Server, ProxyServer


class TestServer(unittest.TestCase):

    def test_attrs(self):
        server = Server()
        self.assertEqual(server.host, 'localhost')
        self.assertEqual(server.port, 4573)

        self.assertEqual(server.warning_connection, 1)
        self.assertEqual(server.handlers, {})


class TestProxyServer(unittest.TestCase):

    def test_attrs(self):
        server = ProxyServer()
        self.assertEqual(server.host, 'localhost')
        self.assertEqual(server.port, 4573)
        self.assertEqual(server.api_url, 'http://localhost/api')
        self.assertIsNone(server.api_auth)

        self.assertEqual(server.warning_connection, 1)
        self.assertEqual(server.exclude_params, (
            'agi_network',
            'agi_network_script',
            'agi_request',
        ))
        self.assertIsNone(server.session)
        self.assertEqual(server.base_url, 'http://localhost')
        self.assertEqual(server.base_path, '/api/')
